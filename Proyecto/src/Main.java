import com.diego.proyecto.GUI.Ventana;
import com.diego.proyecto.GUI.VentanaController;
import com.diego.proyecto.GUI.VentanaModel;

/**
 * Created by dos_6 on 05/02/2016.
 */
public class Main {

    public static void main (String args[]) throws Exception {
        Ventana ventana = new Ventana();
        VentanaModel model = new VentanaModel();
        VentanaController ventanaController = new VentanaController(ventana, model);
    }

}

package com.diego.proyecto.Objetos;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
@Entity
@Table(name="empresas")
public class Empresa implements Serializable{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="direccion")
    private String direccion;
    @Column(name="telefono")
    private String telefono;
    @Column(name="fundacion")
    private Date fundacion;
    @Column(name="tipo")
    private String tipo;

    @OneToMany(mappedBy = "empresaAsignada", cascade = CascadeType.ALL)
    private List<Trabajador> listaTrabajadores;


    @ManyToMany
    @JoinTable(name = "empresa_producto",
            joinColumns = {@JoinColumn(name="id_empresa")}, inverseJoinColumns={@JoinColumn(name="id_producto")})
    private List<Producto> productos;


    @OneToMany(mappedBy = "empresa", cascade = CascadeType.DETACH)
    private List<Contrato> listaContratos;

    public Empresa(){
    }

    public Empresa(int id, String nombre, String direccion, String telefono, Date fundacion, String tipo) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fundacion = fundacion;
        this.tipo = tipo;
        this.listaTrabajadores = new ArrayList<>();
        this.productos = new ArrayList<>();
        this.listaContratos = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFundacion() {
        return fundacion;
    }

    public void setFundacion(Date fundacion) {
        this.fundacion = fundacion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Trabajador> getListaTrabajadores() {
        return listaTrabajadores;
    }

    public void setListaTrabajadores(List<Trabajador> listaTrabajadores) {
        this.listaTrabajadores = listaTrabajadores;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }




    @Override
    public String toString() {
        return nombre;
    }
}

package com.diego.proyecto.Objetos;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
@Entity
@Table(name = "trabajadores")
public class Trabajador implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "nacimiento")
    private Date nacimiento;
    @Column(name = "empresa")
    private String empresa;
    @Column(name = "telefono")
    private String telefono;

    @ManyToOne
    @JoinColumn(name = "id_empresa")
    Empresa empresaAsignada;

    @OneToMany(mappedBy = "trabajador",cascade = CascadeType.DETACH)
    private List<Contrato> listaContratos;

    public Trabajador(){}

    public Trabajador(int id, String nombre, String apellidos, Date nacimiento, String empresa, String telefono, Empresa empresaAsignada) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacimiento = nacimiento;
        this.empresa = empresa;
        this.telefono = telefono;
        this.empresaAsignada = empresaAsignada;
        this.listaContratos = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public Empresa getEmpresaAsignada() {
        return empresaAsignada;
    }

    public void setEmpresaAsignada(Empresa empresaAsignada) {
        this.empresaAsignada = empresaAsignada;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Contrato> getListaContratos() {
        return listaContratos;
    }

    public void setListaContratos(List<Contrato> listaContratos) {
        this.listaContratos = listaContratos;
    }

    @Override
    public String toString() {
        return nombre +" "+apellidos;
    }
}

package com.diego.proyecto.Objetos;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by dos_6 on 03/02/2016.
 */
@Entity
@Table(name = "contratos")
public class Contrato implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombreTrabajador")
    private String nombreTrabajador;
    @Column(name = "nombreEmpresa")
    private String nombreEmpresa;

    @ManyToOne
    @JoinColumn(name = "id_empresa")
    private Empresa empresa;
    @ManyToOne
    @JoinColumn(name = "id_trabajador")
    private Trabajador trabajador;

    public Contrato(){
    }

    public Contrato(int id, String nombreTrabajador, String nombreEmpresa, Empresa empresa, Trabajador trabajador) {
        this.id = id;
        this.nombreTrabajador = nombreTrabajador;
        this.nombreEmpresa = nombreEmpresa;
        this.empresa = empresa;
        this.trabajador = trabajador;
    }

    public int getId() {
        return id;
    }

    public String getNombreTrabajador() {
        return nombreTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
}

package com.diego.proyecto.Objetos;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
/**
 * Created by dos_6 on 05/02/2016.
 */
@Entity
@Table(name="productos")
public class Producto implements Serializable{

    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "precio_base")
    private float precioBase;

    @ManyToMany(mappedBy = "productos",cascade = CascadeType.DETACH)
    private List<Empresa> listaEmpresas;

    public Producto(){
        this.listaEmpresas = new ArrayList<>();
    }

    public Producto(int id, String nombre, float precioBase) {
        this.id = id;
        this.nombre = nombre;
        this.precioBase = precioBase;
        this.listaEmpresas = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(float precioBase) {
        this.precioBase = precioBase;
    }

    public List<Empresa> getListaEmpresas() {
        return listaEmpresas;
    }

    public void setListaEmpresas(List<Empresa> listaEmpresas) {
        this.listaEmpresas = listaEmpresas;
    }

    @Override
    public String toString() {
        return nombre;
    }
}

package com.diego.proyecto.GUI;

import javax.swing.*;
import java.awt.event.*;

public class JLogin extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfNombre;
    private JTextField tfPassword;
    private String usuario;
    private String contrasena;

    public JLogin() {
        super();
        setContentPane(contentPane);
        setTitle("Login");
        pack();
        setLocationRelativeTo(null);
        setModal(true);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        if((tfNombre.getText().equals(""))||(tfPassword.equals(""))){
            JOptionPane.showMessageDialog(null, "Debes introducir usuario y contrase?a", "Login", JOptionPane.ERROR_MESSAGE);
            return;
        }
        usuario = tfNombre.getText();
        contrasena = tfPassword.getText();
        setVisible(false);

    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public static void main(String[] args) {
        JLogin dialog = new JLogin();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}

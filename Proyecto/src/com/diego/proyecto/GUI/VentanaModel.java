package com.diego.proyecto.GUI;

import com.diego.proyecto.HibernateUtil;
import com.diego.proyecto.Objetos.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Query;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
public class VentanaModel {

    List<Producto> listaProductos;
    List<Empresa> listaEmpresas;
    List<Trabajador> listaTrabajadores;
    List<Contrato> listaContratos;

    public VentanaModel(){
        listaProductos = new ArrayList<>();
        listaEmpresas = new ArrayList<>();
        listaTrabajadores = new ArrayList<>();
    }

    public void conectar() {
        try {
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void desconectar() {
        try {
            HibernateUtil.closeSessionFactory();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void actualizar(Object objeto)throws HibernateException{
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.update(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public void guardar(Object objeto) throws HibernateException{
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public void borrar(Object objeto){
        Session session = null;
        try {
            session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(objeto);
            session.getTransaction().commit();
            session.close();
        }
        catch (RuntimeException e) {
            session.getTransaction().rollback();
            throw e;
        }
    }

    public boolean getUsuario(String nombre, String password){
        boolean estado = false;


            Query query = HibernateUtil.
                    getCurrentSession().createQuery("FROM Usuario WHERE nombre = :nombre AND password = :password");
            query.setParameter("nombre", nombre);
            query.setParameter("password", password);
            List<Usuario> usuarios = (List<Usuario>) query.list();
            for (Usuario usuario : usuarios) {
                if (usuario.getNombre().equals(nombre) && usuario.getPassword().equals(password)) {
                    estado = true;
                }
            }


        return estado;
    }

    public List<Trabajador> cargarTrabajador(){
        Query query = HibernateUtil.getCurrentSession().createQuery("From Trabajador ");
        listaTrabajadores = query.list();
        return listaTrabajadores;
    }

    public List<Producto> cargarProducto(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Producto ORDER BY precioBase ASC ");
        listaProductos = query.list();

        return listaProductos;
    }

    public List<Empresa> cargarEmpresa(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Empresa ");
        listaEmpresas = query.list();

        return listaEmpresas;
    }

    public Producto getProducto(int id){
        Producto producto = (Producto) HibernateUtil.getCurrentSession().get(Producto.class,id);
        return producto;
    }

    public Empresa getEmpresa(int id){
        Empresa empresa = (Empresa) HibernateUtil.getCurrentSession().get(Empresa.class,id);
        return empresa;
    }

    public Trabajador getTrabajador(int id){
        Trabajador trabajador = (Trabajador) HibernateUtil.getCurrentSession().get(Trabajador.class,id);
        return trabajador;
    }

    public void XMLEmpresa(List<Empresa> vectorEmpresa){

        //Se crea la variable para el documento
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            //Se crea el documento XML
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            //Se crea el elemento raiz
            org.w3c.dom.Element raiz = documento.createElement("Empresas");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoEmpresa = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            //Se crea el nodo padre
            for (Empresa empresa : vectorEmpresa) { // Se recorren los vectores con un bucle.
                nodoEmpresa = documento.createElement("Empresa");
                raiz.appendChild(nodoEmpresa);
                //Se crean los nodos hijos

                nodoDatos = documento.createElement("id");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("tipo");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("direccion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getDireccion());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("telefono");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTelefono());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("fundacion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getFundacion()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("productos");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getProductos()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("trabajadores");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getListaTrabajadores()));
                nodoDatos.appendChild(texto);


            }
            //Se guardan los datos en un documento XML
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"empresa.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public void XMLTrabajador(List<Trabajador> vectorTrabajadores){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Trabajadores");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoTrabajador = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (Trabajador trabajador : vectorTrabajadores) {
                nodoTrabajador = documento.createElement("Trabajador");
                raiz.appendChild(nodoTrabajador);

                nodoDatos = documento.createElement("id");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("apellidos");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getApellidos());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("empresa");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getEmpresa()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nacimiento");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getNacimiento()));
                nodoDatos.appendChild(texto);



            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"trabajadores.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public void XMLProducto(List<Producto> vectorProductos){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Productos");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoProducto = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (Producto producto : vectorProductos) {
                nodoProducto = documento.createElement("Producto");
                raiz.appendChild(nodoProducto);

                nodoDatos = documento.createElement("id");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(producto.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(producto.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("precio_base");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(producto.getPrecioBase()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("empresas_productoras");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(producto.getListaEmpresas()));
                nodoDatos.appendChild(texto);

            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"productos.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }
}

package com.diego.proyecto.GUI;

import com.toedter.calendar.JDateChooser;
import org.swixml.SwingEngine;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * GUI Principal
 */
public class Ventana extends JFrame{
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JTextField tfEmpresaNombre;
    public JTextField tfEmpresaDireccion;
    public JTextField tfEmpresaTelefono;
    public JComboBox cbEmpresaTipo;
    public JButton btEmpresaNuevo;
    public JButton btEmpresaModificar;
    public JButton btEmpresaGuardar;
    public JDateChooser dcEmpresaFundacion;
    public JButton btEmpresaCancelar;
    public JButton btEmpresaEliminar;
    public JTextField tfProductoNombre;
    public JTextField tfProductoPrecio;
    public JButton btProductoNuevo;
    public JButton btProductoModificar;
    public JButton btProductoGuardar;
    public JButton btProductoCancelar;
    public JButton btProductoEliminar;
    public JTextField tfTrabajadorNombre;
    public JTextField tfTrabajadorApellidos;
    public JTextField tfTrabajadorTelefono;
    public JComboBox cbTrabajadorEmpresa;
    public JTable tablaTrabajador;
    public JButton btTrabajadorNuevo;
    public JButton btTrabajadorModificar;
    public JButton btTrabajadorGuardar;
    public JButton btTrabajadorCancelar;
    public JButton btTrabajadorEliminar;
    public JComboBox cbContratoTrabajador;
    public JComboBox cbContratoEmpresa;
    public JTable tablaContrato;
    public JButton btContratoNuevo;
    public JButton btContratoModificar;
    public JButton btContratoGuardar;
    public JButton btContratoCancelar;
    public JButton btContratoEliminar;
    public JMenuBar menuBar;
    public JTable tablaProducto;
    public JTable tablaEmpresa;
    public JLabel lbEstado;
    public JDateChooser dtTrabajadorNacimiento;
    public JComboBox cbProduccionEmpresa;
    public JTable tablaProduccion;
    public JComboBox cbProduccionProducto;
    public JButton btProduccionAddProducto;
    private JSplitPane splitPane;
    public JList listaProductos;
    public JTextField tfBuscarProducto;
    public JTextField tfBuscarEmpresa;
    public JTextField tfBuscarTrabajador;
    public JTextField tfBuscarContrato;
    public JComboBox cbProductoBusqueda;
    public JComboBox cbEmpresaBusqueda;
    public JComboBox cbContratoBusqueda;
    public JComboBox cbTrabajadorBusqueda;
    public JMenu menuArchivo;
    public JMenu menuOpciones;
    public JMenuItem botonImportarXML;
    public JMenuItem botonExportarXML;
    public JMenuItem botonAyuda;
    public JMenuItem botonConfiguracion;
    public JMenuItem botonConectar;
    public JMenuItem botonAcercaDe;
    public JMenuItem botonDesconectar;
    public JMenuItem botonSalir;
    public DefaultTableModel modeloTablaProducto;
    public DefaultTableModel modeloTablaEmpresa;
    public DefaultTableModel modeloTablaTrabajador;
    public DefaultTableModel modeloTablaContrato;
    public DefaultTableModel modeloTablaProduccion;
    public DefaultListModel modeloListaProduccion;

    public Ventana() throws Exception{
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        setSize((int) width, (int) height);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        iniciarBotones(true);
        setJMenuBar(menuBar());
        iniciarTablas();
        iniciarCombos();
        cbEmpresaTipo.addItem("SL");
        cbEmpresaTipo.addItem("SA");
        Font font = new Font("Ubuntu", Font.BOLD, 18);
        lbEstado.setFont(font);
        setVisible(true);
    }

    public JMenuBar menuBar(){
        menuBar = new JMenuBar();

        menuArchivo = new JMenu("Archivo");
        menuOpciones = new JMenu("Opciones");

        menuBar.add(menuArchivo);
        menuBar.add(menuOpciones);

        botonExportarXML = new JMenuItem("Exportar XML");
        botonImportarXML = new JMenuItem("Importar XML");
        botonConectar = new JMenuItem("Conectar");
        botonDesconectar = new JMenuItem("Desconectar");
        botonSalir = new JMenuItem("Salir");

        menuArchivo.add(botonConectar);
        menuArchivo.add(botonDesconectar);
        menuArchivo.add(botonExportarXML);
        menuArchivo.add(botonImportarXML);
        menuArchivo.add(botonSalir);

        botonAcercaDe = new JMenuItem("Acerca de...");
        botonConfiguracion = new JMenuItem("Configuración");
        botonAyuda = new JMenuItem("Ayuda");
        menuOpciones.add(botonConfiguracion);
        menuOpciones.add(botonAyuda);
        menuOpciones.add(botonAcercaDe);

        return menuBar;
    }

    public void iniciarBotones(boolean estado){
        btProductoEliminar.setEnabled(!estado);
        btProductoNuevo.setEnabled(!estado);
        btProductoGuardar.setEnabled(!estado);
        btProductoModificar.setEnabled(!estado);
        btProductoCancelar.setEnabled(!estado);
        tfProductoNombre.setEnabled(!estado);
        tfProductoPrecio.setEnabled(!estado);

        btEmpresaEliminar.setEnabled(!estado);
        btEmpresaNuevo.setEnabled(!estado);
        btEmpresaGuardar.setEnabled(!estado);
        btEmpresaModificar.setEnabled(!estado);
        btEmpresaCancelar.setEnabled(!estado);
        tfEmpresaNombre.setEnabled(!estado);
        tfEmpresaTelefono.setEnabled(!estado);
        tfEmpresaDireccion.setEnabled(!estado);
        cbEmpresaTipo.setEnabled(!estado);
        dcEmpresaFundacion.setEnabled(!estado);

        btTrabajadorEliminar.setEnabled(!estado);
        btTrabajadorNuevo.setEnabled(!estado);
        btTrabajadorGuardar.setEnabled(!estado);
        btTrabajadorModificar.setEnabled(!estado);
        btTrabajadorCancelar.setEnabled(!estado);
        dtTrabajadorNacimiento.setEnabled(!estado);
        tfTrabajadorNombre.setEnabled(!estado);
        tfTrabajadorApellidos.setEnabled(!estado);
        tfTrabajadorTelefono.setEnabled(!estado);
        cbTrabajadorEmpresa.setEnabled(!estado);

       /* cbContratoEmpresa.setEnabled(!estado);
        cbContratoTrabajador.setEnabled(!estado);
        btContratoNuevo.setEnabled(!estado);
        btContratoGuardar.setEnabled(!estado);
        btContratoModificar.setEnabled(!estado);
        btContratoCancelar.setEnabled(!estado);
        btContratoEliminar.setEnabled(!estado);*/

    }

    public void iniciarTablas(){
        String[] productos = new String[]{"id","nombre","precio"};
        String[] empresas = new String[]{"id","nombre","tipo","direccion","telefono","fundacion"};
        String[] trabajadores = new String[]{"id","nombre","apellidos","telefono","empresa","nacimiento"};
        String[] contratos = new String[]{"id","trabajador","empresa"};
        String[] produccion = new String[]{"producto","precio base"};

        modeloTablaProducto = new DefaultTableModel(productos,0);
        tablaProducto.setModel(modeloTablaProducto);

        modeloTablaEmpresa = new DefaultTableModel(empresas,0);
        tablaEmpresa.setModel(modeloTablaEmpresa);

        modeloTablaTrabajador = new DefaultTableModel(trabajadores,0);
        tablaTrabajador.setModel(modeloTablaTrabajador);

        /*modeloTablaContrato = new DefaultTableModel(contratos,0);
        tablaContrato.setModel(modeloTablaContrato);*/

        modeloTablaProduccion = new DefaultTableModel(produccion,0);
        tablaProduccion.setModel(modeloTablaProduccion);

        modeloListaProduccion = new DefaultListModel();
        listaProductos.setModel(modeloListaProduccion);

    }

    public void iniciarCombos(){
        String[] productos = new String[]{"nombre"};
        String[] empresas = new String[]{"nombre","tipo","direccion","telefono"};
        String[] trabajadores = new String[]{"nombre","apellidos","telefono","empresa"};
       // String[] contratos = new String[]{"trabajador","empresa"};

        for(String a : productos)
            cbProductoBusqueda.addItem(a);
        for(String a : empresas)
            cbEmpresaBusqueda.addItem(a);
        for(String a : trabajadores)
            cbTrabajadorBusqueda.addItem(a);
        /*for(String a : contratos)
            cbContratoBusqueda.addItem(a);*/
    }
}

package com.diego.proyecto.GUI;

import com.diego.proyecto.HibernateUtil;
import com.diego.proyecto.Objetos.Contrato;
import com.diego.proyecto.Objetos.Empresa;
import com.diego.proyecto.Objetos.Producto;
import com.diego.proyecto.Objetos.Trabajador;
import com.diego.proyecto.Util;
import javassist.tools.rmi.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.bind.SchemaOutputResolver;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
public class VentanaController{

    Ventana view;
    VentanaModel model;
    Producto producto;
    Empresa empresa;
    Trabajador trabajador;
    Contrato contrato;
    Util util;
    public Exception e;

    public VentanaController(Ventana view, VentanaModel model) {
        this.view = view;
        this.model = model;
        listeners();
        model.conectar();
    }

    public VentanaController(){}

    public void inicializar(){
        apagarBotones(true);
        listarProducto();
        listarEmpresa();
        listarTrabajadores();
        listarProduccionEmpresa();
    }

    public void desconectar(){
        model.desconectar();
    }

    public void listeners(){
        view.btProductoNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               // model.iniciarBotonProducto(false);
                iniciarBotonProducto(false);
            }
        });

        view.btProductoGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                producto = new Producto();
                producto.setNombre(view.tfProductoNombre.getText());
                producto.setPrecioBase(Float.parseFloat(view.tfProductoPrecio.getText()));
                model.guardar(producto);
                listarProducto(model.listaProductos);
                iniciarBotonProducto(false);
                view.lbEstado.setText("Producto guardado con exito");
            }
        });

        view.btProductoModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int fila = view.tablaProducto.getSelectedRow();
                int id = (int) view.tablaProducto.getValueAt(fila,0);
                System.out.println(id);
                producto = model.getProducto(id);
                producto.setNombre(view.tfProductoNombre.getText());
                producto.setPrecioBase(Float.parseFloat(view.tfProductoPrecio.getText()));
                if(util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.actualizar(producto);
                view.lbEstado.setText("Producto actualizado correctamente");
                listarProducto();
            }
        });

        view.btProductoCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.tfProductoNombre.setText("");
                view.tfProductoPrecio.setText("0");
            }
        });

        view.btProductoEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaProducto.getSelectedRow();
                int id = (int) view.tablaProducto.getValueAt(fila,0);
                producto = model.getProducto(id);
                model.borrar(producto);
                view.lbEstado.setText("Producto eliminado correctamente");
                listarProducto();
            }
        });

        view.btEmpresaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iniciarBotonEmpresa(false);
            }
        });

        view.btEmpresaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                empresa = new Empresa();
                empresa.setNombre(view.tfEmpresaNombre.getText());
                empresa.setTipo(view.cbEmpresaTipo.getSelectedItem().toString());
                empresa.setDireccion(view.tfEmpresaDireccion.getText());
                empresa.setTelefono(view.tfEmpresaTelefono.getText());
                empresa.setFundacion(view.dcEmpresaFundacion.getDate());
                model.guardar(empresa);
                view.lbEstado.setText("Empresa guardada correctamente");

                iniciarBotonEmpresa(true);
                listarEmpresa();

            }
        });

        view.btEmpresaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaEmpresa.getSelectedRow();
                int id = (int) view.tablaEmpresa.getValueAt(fila,0);
                Empresa empresa = model.getEmpresa(id);
                empresa.setNombre(view.tfEmpresaNombre.getText());
                empresa.setTipo(view.cbEmpresaTipo.getSelectedItem().toString());
                empresa.setDireccion(view.tfEmpresaDireccion.getText());
                empresa.setTelefono(view.tfEmpresaTelefono.getText());
                empresa.setFundacion(view.dcEmpresaFundacion.getDate());
                if(util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.actualizar(empresa);
                listarEmpresa();
                view.lbEstado.setText("Empresa actualizada correctamente");
            }
        });

        view.btEmpresaCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                view.tfEmpresaNombre.setText("");
                view.tfEmpresaTelefono.setText("");
                view.tfEmpresaDireccion.setText("");
                view.dcEmpresaFundacion.setDate(null);
            }
        });

        view.btEmpresaEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaEmpresa.getSelectedRow();
                int id = (int) view.tablaEmpresa.getValueAt(fila,0);
                empresa = model.getEmpresa(id);
                model.borrar(empresa);
                view.lbEstado.setText("Empresa eliminada correctamente");
                listarEmpresa();
            }
        });

        view.btTrabajadorNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inicarBotonTrabajador(false);
            }
        });

        view.btTrabajadorGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                trabajador = new Trabajador();
                trabajador.setNombre(view.tfTrabajadorNombre.getText());
                trabajador.setTelefono(view.tfTrabajadorTelefono.getText());
                trabajador.setApellidos(view.tfTrabajadorApellidos.getText());
                trabajador.setNacimiento(view.dtTrabajadorNacimiento.getDate());
                empresa = (Empresa)view.cbTrabajadorEmpresa.getSelectedItem();
                trabajador.setEmpresaAsignada(empresa);
                trabajador.setEmpresa(empresa.getNombre());
                System.out.println(empresa);
                model.guardar(trabajador);
                view.lbEstado.setText("Trabajador guardado con exito");
                listarTrabajadores();

            }
        });

        view.btTrabajadorModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int fila = view.tablaTrabajador.getSelectedRow();
                int id = (int) view.tablaTrabajador.getValueAt(fila,0);
                trabajador = model.getTrabajador(id);
                trabajador.setNombre(view.tfTrabajadorNombre.getText());
                trabajador.setApellidos(view.tfTrabajadorApellidos.getText());
                trabajador.setNacimiento(view.dtTrabajadorNacimiento.getDate());
                trabajador.setTelefono(view.tfTrabajadorTelefono.getText());
                empresa = (Empresa)view.cbTrabajadorEmpresa.getSelectedItem();
                trabajador.setEmpresaAsignada(empresa);
                trabajador.setEmpresa(empresa.getNombre());
                model.actualizar(trabajador);
                view.cbContratoTrabajador.removeAllItems();
                view.lbEstado.setText("Trabajador modificado con exito");
                listarTrabajadores();
            }
        });

        view.btTrabajadorCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                view.tfTrabajadorNombre.setText("");
                view.tfTrabajadorTelefono.setText("");
                view.tfTrabajadorTelefono.setText("");
                view.dtTrabajadorNacimiento.setDate(null);

            }
        });

        view.btTrabajadorEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaTrabajador.getSelectedRow();
                int id = (int) view.tablaTrabajador.getValueAt(fila,0);
                trabajador = model.getTrabajador(id);
                try{
                    model.borrar(trabajador);
                }catch (Exception ex){
                    view.lbEstado.setText("Error de algo");
                }

                //view.cbContratoTrabajador.removeAllItems();
                view.lbEstado.setText("Trabajador eliminado con exito");
                listarTrabajadores();
            }
        });

        view.btProduccionAddProducto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

             Empresa empresa = (Empresa) view.cbProduccionEmpresa.getSelectedItem();
                List<Producto> cosas = view.listaProductos.getSelectedValuesList();
                empresa.getProductos().clear();
                empresa.setProductos(cosas);
              /*  for(Producto producto : cosas){
                    empresa.getProductos().add(producto);
                }*/
                model.actualizar(empresa);
                listarProducto();
                view.lbEstado.setText("Productos añadidos con exito");
            }
        });

        view.botonConectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    login();

            }
        });

        view.botonDesconectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    desconectar();
                    apagarBotones(true);
                    view.lbEstado.setText("Desconectado Correctamente");
            }
        });

        view.botonExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.XMLEmpresa(model.cargarEmpresa());
                model.XMLTrabajador(model.cargarTrabajador());
                model.XMLProducto(model.cargarProducto());
            }
        });

        view.botonImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.botonConfiguracion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.botonAyuda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.botonAcercaDe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        view.botonSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                desconectar();
                System.exit(JFrame.EXIT_ON_CLOSE);
            }
        });

        view.tablaProducto.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaProducto.getSelectedRow() == -1)
                    return;
                int fila = view.tablaProducto.getSelectedRow();
                int id = (Integer.parseInt(String.valueOf(view.tablaProducto.getValueAt(fila, 0))));
                producto = model.getProducto(id);
                view.tfProductoNombre.setText(producto.getNombre());
                view.tfProductoPrecio.setText(String.valueOf(producto.getPrecioBase()));
            }
        });

        view.tablaEmpresa.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaEmpresa.getSelectedRow() == -1)
                    return;
                int fila = view.tablaEmpresa.getSelectedRow();
                int id = (int) view.tablaEmpresa.getValueAt(fila,0);
                empresa = model.getEmpresa(id);
                view.tfEmpresaNombre.setText(empresa.getNombre());
                view.tfEmpresaTelefono.setText(empresa.getTelefono());
                view.tfEmpresaDireccion.setText(empresa.getDireccion());
                view.cbEmpresaTipo.setSelectedItem(empresa.getTipo());
                view.dcEmpresaFundacion.setDate(empresa.getFundacion());
            }
        });

        view.tablaTrabajador.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaTrabajador.getSelectedRow() == -1)
                    return;

                int fila = view.tablaTrabajador.getSelectedRow();
                int id = (int) view.tablaTrabajador.getValueAt(fila,0);
                trabajador = model.getTrabajador(id);
                view.tfTrabajadorNombre.setText(trabajador.getNombre());
                view.tfTrabajadorApellidos.setText(trabajador.getApellidos());
                view.tfTrabajadorTelefono.setText(trabajador.getTelefono());
                view.cbTrabajadorEmpresa.setSelectedItem(trabajador.getEmpresaAsignada().getNombre());
                view.dtTrabajadorNacimiento.setDate(trabajador.getNacimiento());
            }
        });

       /* view.tablaContrato.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaContrato.getSelectedRow() == -1)
                    return;

                int fila = view.tablaContrato.getSelectedRow();
                int id = (int) view.tablaContrato.getValueAt(fila,0);
                contrato = model.getContrato(id);
                view.cbContratoEmpresa.setSelectedItem(contrato.getEmpresa());
                view.cbContratoTrabajador.setSelectedItem(contrato.getTrabajador());

            }


        });*/

        view.cbProduccionEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(view.cbProduccionEmpresa.getSelectedItem() == null)
                    return;
                listarProduccionEmpresa();
               /* empresa = (Empresa) view.cbProduccionEmpresa.getSelectedItem();
                List<Producto> listaProductos = empresa.getProductos();
                view.modeloTablaProduccion.setNumRows(0);
                for(Producto producto : listaProductos){
                    Object[] fila = new Object[]{producto.getNombre(),producto.getPrecioBase()};
                    view.modeloTablaProduccion.addRow(fila);
                }*/
            }
        });

        view.tfBuscarProducto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (view.tfBuscarProducto.getText().length() > 0) {
                    Session session = HibernateUtil.getCurrentSession();
                    Query query;

                    List<Producto> productoList = new ArrayList<Producto>();
                    String buscar = view.tfBuscarProducto.getText();
                    String criterio = view.cbProductoBusqueda.getSelectedItem().toString();

                    switch (criterio) {
                        case "nombre":
                            query = session.createQuery("FROM Producto WHERE nombre LIKE :nombre");
                            query.setParameter("nombre", "%" + buscar + "%");
                            productoList = query.list();
                            listarProducto(productoList);
                            break;
                    }
                }else{
                    listarProducto();
                }
            }
        });

        view.tfBuscarEmpresa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(view.tfBuscarEmpresa.getText().length() > 0){
                    Session session = HibernateUtil.getCurrentSession();
                    Query query;

                    List<Empresa> empresaList = new ArrayList<Empresa>();
                    String buscar = view.tfBuscarEmpresa.getText();
                    String criterio = (String) view.cbEmpresaBusqueda.getSelectedItem();

                    switch (criterio){
                        case "nombre":
                            query = session.createQuery("FROM Empresa WHERE nombre like :nombre");
                            query.setParameter("nombre", "%"+buscar+"%");
                            empresaList = query.list();
                            listarBusquedaEmpresa(empresaList);
                            break;
                        case "tipo":
                            query = session.createQuery("FROM Empresa WHERE tipo like :tipo");
                            query.setParameter("tipo", "%"+buscar+"%");
                            empresaList = query.list();
                            listarBusquedaEmpresa(empresaList);
                            break;
                        case "direccion":
                            query = session.createQuery("FROM Empresa WHERE direccion like :direccion");
                            query.setParameter("direccion", "%"+buscar+"%");
                            empresaList = query.list();
                            listarBusquedaEmpresa(empresaList);
                            break;
                        case "telefono":
                            query = session.createQuery("FROM Empresa WHERE telefono like :telefono");
                            query.setParameter("telefono", "%"+buscar+"%");
                            empresaList = query.list();
                            listarBusquedaEmpresa(empresaList);
                            break;
                    }

                }else {
                    listarEmpresa();
                }
            }
        });

        view.tfBuscarTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(view.tfBuscarTrabajador.getText().length() > 0){
                    Session session = HibernateUtil.getCurrentSession();
                    Query query;

                    List<Trabajador> trabajadorList = new ArrayList<Trabajador>();
                    String buscar = view.tfBuscarTrabajador.getText();
                    String criterio = (String) view.cbTrabajadorBusqueda.getSelectedItem();

                    switch (criterio){
                        case "nombre":
                            query = session.createQuery("FROM Trabajador WHERE nombre like :nombre");
                            query.setParameter("nombre", "%"+buscar+"%");
                            trabajadorList = query.list();
                            listarBusquedaTrabajador(trabajadorList);
                            break;
                        case "apellidos":
                            query = session.createQuery("FROM Trabajador WHERE apellidos like :apellidos");
                            query.setParameter("apellidos", "%"+buscar+"%");
                            trabajadorList = query.list();
                            listarBusquedaTrabajador(trabajadorList);
                            break;
                        case "telefono":
                            query = session.createQuery("FROM Trabajador WHERE telefono like :telefono");
                            query.setParameter("telefono", "%"+buscar+"%");
                            trabajadorList = query.list();
                            listarBusquedaTrabajador(trabajadorList);
                            break;
                        case "empresa":
                            query = session.createQuery("FROM Trabajador WHERE empresa like :empresa");
                            query.setParameter("empresa", "%"+buscar+"%");
                            trabajadorList = query.list();
                            listarBusquedaTrabajador(trabajadorList);
                            break;
                    }
                }
            }
        });
    }

    public void iniciarBotonProducto(boolean estado){
        view.btProductoEliminar.setEnabled(!estado);
        view.btProductoNuevo.setEnabled(estado);
        view.btProductoGuardar.setEnabled(!estado);
        view. btProductoModificar.setEnabled(!estado);
        view. btProductoCancelar.setEnabled(!estado);
        view.tfProductoNombre.setEnabled(!estado);
        view.tfProductoPrecio.setEnabled(!estado);
    }

    public void iniciarBotonEmpresa(boolean estado){
        view. btEmpresaEliminar.setEnabled(!estado);
        view. btEmpresaNuevo.setEnabled(estado);
        view.btEmpresaGuardar.setEnabled(!estado);
        view.btEmpresaModificar.setEnabled(!estado);
        view. btEmpresaCancelar.setEnabled(!estado);
        view.tfEmpresaNombre.setEnabled(!estado);
        view.tfEmpresaTelefono.setEnabled(!estado);
        view.tfEmpresaDireccion.setEnabled(!estado);
        view.cbEmpresaTipo.setEnabled(!estado);
        view.dcEmpresaFundacion.setEnabled(!estado);
    }

    public void inicarBotonTrabajador(boolean estado){
        view.btTrabajadorEliminar.setEnabled(!estado);
        view.btTrabajadorNuevo.setEnabled(estado);
        view.btTrabajadorGuardar.setEnabled(!estado);
        view.btTrabajadorModificar.setEnabled(!estado);
        view. btTrabajadorCancelar.setEnabled(!estado);
        view.tfTrabajadorNombre.setEnabled(!estado);
        view.tfTrabajadorApellidos.setEnabled(!estado);
        view.tfTrabajadorTelefono.setEnabled(!estado);
        view.cbTrabajadorEmpresa.setEnabled(!estado);
        view.dtTrabajadorNacimiento.setEnabled(!estado);
    }

    /*public void iniciarBotonContrato(boolean estado){
        view. btContratoEliminar.setEnabled(!estado);
        view.btContratoNuevo.setEnabled(estado);
        view. btContratoGuardar.setEnabled(!estado);
        view.btContratoModificar.setEnabled(!estado);
        view.btContratoCancelar.setEnabled(!estado);
        view.cbContratoEmpresa.setEnabled(!estado);
        view.cbContratoTrabajador.setEnabled(!estado);
    }*/

    public void apagarBotones(boolean estado){
        view.btProductoEliminar.setEnabled(!estado);
        view.btProductoNuevo.setEnabled(estado);
        view.btProductoGuardar.setEnabled(!estado);
        view. btProductoModificar.setEnabled(!estado);
        view. btProductoCancelar.setEnabled(!estado);
        view.tfProductoNombre.setEnabled(!estado);
        view.tfProductoPrecio.setEnabled(!estado);

        view. btEmpresaEliminar.setEnabled(!estado);
        view. btEmpresaNuevo.setEnabled(estado);
        view.btEmpresaGuardar.setEnabled(!estado);
        view.btEmpresaModificar.setEnabled(!estado);
        view. btEmpresaCancelar.setEnabled(!estado);
        view.tfEmpresaNombre.setEnabled(!estado);
        view.tfEmpresaTelefono.setEnabled(!estado);
        view.tfEmpresaDireccion.setEnabled(!estado);
        view.cbEmpresaTipo.setEnabled(!estado);
        view.dcEmpresaFundacion.setEnabled(!estado);

        view.btTrabajadorEliminar.setEnabled(!estado);
        view.btTrabajadorNuevo.setEnabled(estado);
        view.btTrabajadorGuardar.setEnabled(!estado);
        view.btTrabajadorModificar.setEnabled(!estado);
        view.btTrabajadorCancelar.setEnabled(!estado);
        view.dtTrabajadorNacimiento.setEnabled(!estado);
        view.tfTrabajadorNombre.setEnabled(!estado);
        view.tfTrabajadorApellidos.setEnabled(!estado);
        view.tfTrabajadorTelefono.setEnabled(!estado);
        view.cbTrabajadorEmpresa.setEnabled(!estado);

        /*view.cbContratoEmpresa.setEnabled(!estado);
        view.cbContratoTrabajador.setEnabled(!estado);
        view.btContratoNuevo.setEnabled(estado);
        view.btContratoGuardar.setEnabled(!estado);
        view.btContratoModificar.setEnabled(!estado);
        view.btContratoCancelar.setEnabled(!estado);
        view.btContratoEliminar.setEnabled(!estado);*/

    }

    public void listarProduccionEmpresa(){
        try {
            empresa = (Empresa) view.cbProduccionEmpresa.getSelectedItem();
            List<Producto> listaProductos = empresa.getProductos();
            view.modeloTablaProduccion.setNumRows(0);
            if (listaProductos.size() == 0) return;
            if (listaProductos != null) {
                for (Producto producto : listaProductos) {
                    Object[] fila = new Object[]{producto.getNombre(), producto.getPrecioBase()};
                    view.modeloTablaProduccion.addRow(fila);

                }
            }
        }catch (Exception e){
            System.out.println("Error listarProduccionEmpresa()");
        }
    }

    public void listarProducto(){
        List<Producto> filaProducto = model.cargarProducto();
        view.modeloListaProduccion.removeAllElements();
        view.modeloTablaProducto.setNumRows(0);
        if(filaProducto != null){
            for(Producto producto : filaProducto){

                Object[] fila = new Object[]{producto.getId(),producto.getNombre(),util.formatMoneda(producto.getPrecioBase())};

                view.modeloTablaProducto.addRow(fila);
                view.modeloListaProduccion.addElement(producto);
            }
        }
        if(filaProducto.size()>0){
            view.btProductoModificar.setEnabled(true);
        }

    }

    public void listarProducto(List<Producto> productosList){
        view.modeloListaProduccion.removeAllElements();
        view.modeloTablaProducto.setNumRows(0);
        if(productosList != null){
            for(Producto producto : productosList){

                Object[] fila = new Object[]{producto.getId(),producto.getNombre(),util.formatMoneda(producto.getPrecioBase())};

                view.modeloTablaProducto.addRow(fila);
                view.modeloListaProduccion.addElement(producto);
            }
        }
        /*if(productosList.size()>0){
            view.btProductoModificar.setEnabled(true);
        }*/

    }

    public void listarBusquedaEmpresa(List<Empresa> empresaList){
        System.out.println(empresaList);
        view.modeloTablaEmpresa.setNumRows(0);
        view.cbTrabajadorEmpresa.removeAllItems();
        view.cbContratoEmpresa.removeAllItems();
        view.cbProduccionEmpresa.removeAllItems();
        if(empresaList != null){
            for(Empresa empresa : empresaList){
                Object[] fila = new Object[]{empresa.getId(),empresa.getNombre(),empresa.getTipo(),
                        empresa.getDireccion(),empresa.getTelefono(),empresa.getFundacion()};

                view.modeloTablaEmpresa.addRow(fila);
                view.cbTrabajadorEmpresa.addItem(empresa);
                view.cbContratoEmpresa.addItem(empresa);
                view.cbProduccionEmpresa.addItem(empresa);
            }
        }
    }

    public void listarBusquedaTrabajador(List<Trabajador> filaTrabajador){
        view.modeloTablaTrabajador.setNumRows(0);
        try {
            if (filaTrabajador != null) {
                for (Trabajador trabajador : filaTrabajador) {
                    Object[] fila = new Object[]{trabajador.getId(), trabajador.getNombre(), trabajador.getApellidos(), trabajador.getTelefono(),
                            trabajador.getEmpresa(), trabajador.getNacimiento()};
                    view.modeloTablaTrabajador.addRow(fila);
                    view.cbContratoTrabajador.addItem(trabajador);
                }
            }else {
                view.lbEstado.setText("No hay trabajadores registrados");
            }
        }catch (Exception e){
            view.lbEstado.setText("Algo ha pasado :(");
        }
    }

   /* public void listarBusquedaContrato(List<Contrato> filaContrato){
        try {
            view.modeloTablaContrato.setNumRows(0);
            if (filaContrato != null) {
                for (Contrato contrato : filaContrato) {
                    Object[] fila = new Object[]{contrato.getId(), contrato.getTrabajador().getNombre(), contrato.getEmpresa().getNombre()};
                    view.modeloTablaContrato.addRow(fila);
                }
            }
        }catch (Exception e){
            view.lbEstado.setText("Algo ha pasado");
        }
    }*/

    public void listarTrabajadores(){
        try {
            List<Trabajador> filaTrabajador = model.cargarTrabajador();
            for (Trabajador trabajador : filaTrabajador) {
                System.out.println(trabajador.getNombre());
            }
            view.modeloTablaTrabajador.setNumRows(0);
           // view.cbContratoTrabajador.removeAllItems();
            if (filaTrabajador != null) {
                for (Trabajador trabajador : filaTrabajador) {
                    Object[] fila = new Object[]{trabajador.getId(), trabajador.getNombre(), trabajador.getApellidos(), trabajador.getTelefono(),
                            trabajador.getEmpresa(), trabajador.getNacimiento()};
                    view.modeloTablaTrabajador.addRow(fila);
                  //  view.cbContratoTrabajador.addItem(trabajador);
                }
            }else {
                view.lbEstado.setText("No hay trabajadores registrados");
            }

        }catch (Exception e){
            view.lbEstado.setText("Algo ha pasado :(");
        }

      /*  */
    }

    public void listarEmpresa(){
        List<Empresa> filaEmpresa = model.cargarEmpresa() ;
        System.out.println(filaEmpresa);
        view.modeloTablaEmpresa.setNumRows(0);
        view.cbTrabajadorEmpresa.removeAllItems();
//        view.cbContratoEmpresa.removeAllItems();
        view.cbProduccionEmpresa.removeAllItems();
        if(filaEmpresa != null){
            for(Empresa empresa : filaEmpresa){
                Object[] fila = new Object[]{empresa.getId(),empresa.getNombre(),empresa.getTipo(),
                        empresa.getDireccion(),empresa.getTelefono(),empresa.getFundacion()};

                view.modeloTablaEmpresa.addRow(fila);
                view.cbTrabajadorEmpresa.addItem(empresa);
              //  view.cbContratoEmpresa.addItem(empresa);
                view.cbProduccionEmpresa.addItem(empresa);
            }
        }

       /* if(filaEmpresa.size()>0){
            view.btTrabajadorModificar.setEnabled(true);
        }*/
    }

   /* public void listarContrato(){
        try {
            List<Contrato> filaContrato = model.cargarContrato();
            view.modeloTablaContrato.setNumRows(0);
            if (filaContrato != null) {
                for (Contrato contrato : filaContrato) {
                    Object[] fila = new Object[]{contrato.getId(), contrato.getTrabajador().getNombre(), contrato.getEmpresa().getNombre()};
                    view.modeloTablaContrato.addRow(fila);
                }
            }
        }catch (Exception e){
            view.lbEstado.setText("Algo ha pasado");
        }
    }*/

    private void login(){
        JLogin login = new JLogin();
        login.setVisible(true);

        String usuario = login.getUsuario();
        String contrasena = login.getContrasena();

        try{
            if(model.getUsuario(usuario,contrasena) == true){
                inicializar();

               /* view.btProductoNuevo.setEnabled(true);
                view.btEmpresaNuevo.setEnabled(true);
                view.btTrabajadorNuevo.setEnabled(true);
                //view.btContratoNuevo.setEnabled(true);*/
                view.lbEstado.setText("Conectado");
            }else{
                JOptionPane.showMessageDialog(null, "Datos incorrectos","Login", JOptionPane.ERROR_MESSAGE);
                login.setVisible(true);
                return;
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

}
